*----------------------------------------------------------------------------------
*-----------------------------------------------------
* Analysis 1, 
* Gold standard =  urine collection over the week 
* In this program, we explored the effect of pooling when all the samples were used to make the pool 
*-----------------------------------------------------
*----------------------------------------------------------------------------------


* Open the wide dataset 
use "pooling validation wide dataset 2019_09_11_wide.dta", clear


*-----------------------------------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
*  A) Below we estimated the urinary concentrations in the gold standdard and each type of pools
*-----------------------------------------------------
*-----------------------------------------------------------------------------------------------------------------------------------------------------------


*  Gold standard, 	Conc = sum(conci*Volumei)/ sum(Volumei) *
*----------------------------------------------------------------

egen Vol_168h = rowtotal(TOTAL_VOLUME*)
label var Vol_168h "total volume of urine produced over 168h"
list TOTAL_VOLUME* Vol_168h

foreach biom in TRICLOSAN BPA CREATININE SG {
	forvalues i = 1(1)72 {
		gen `biom'_Q_`i' = `biom'`i'*TOTAL_VOLUME`i'
	}
		
	egen `biom'_Q = rowtotal(`biom'_Q_*) /* theorical BPA quantity in the 168 hour samples */
	drop `biom'_Q_*

	gen G_168H_`biom' = `biom'_Q / Vol_168h
	label var G_168H_`biom' "Gold standard `biom' conc in 168 hour urine sample"
	drop `biom'_Q 
}



* Volume based pools  
*----------------------
 
* representation of each sample in the final pool depends on the sample volume and total volume of urine produced : Prop =Vol_samplei / Vol_24h 
forvalues i = 1(1)72 {
	gen prop_S`i' = TOTAL_VOLUME`i' / Vol_168h
}


foreach biom in TRICLOSAN BPA CREATININE SG {
	forvalues i = 1(1)72 {
		gen Q_`biom'_equalvol`i' = `biom'`i'* prop_S`i'
	}
		
	egen VBP_168H_`biom'_ = rowtotal(Q_`biom'_equalvol*)
	label var VBP_168H_`biom' "conc in volume based pool"

	drop Q_`biom'_equalvol*
	corr VBP_168H_`biom' G_168H_`biom'
}


 * equal volume pool 
*----------------------
  egen N_sample = rownonmiss(BPA*) /* Step 1:  * Number of urine sample per women */ 
 tab N_sample 
 
 foreach biom in TRICLOSAN BPA CREATININE SG {
	 egen `biom'_Q_xML = rowtotal(`biom'*)
	 gen EVP_168H_`biom' = `biom'_Q_xML/N_sample
	 drop `biom'_Q_xML
 }
 
 
  *  equal volume pool corrected for creatinine concentration
  *-------------------------------------------------------------

   foreach biom in TRICLOSAN BPA {
		gen EVP_168H_creat_`biom' = EVP_168H_`biom' /  EVP_168H_CREATININE
  }
  
  
  
 /* equal volume pool corrected for SG:   The following formula was used to correct urinary concentrations for specific gravity:
CSG = C × [(SGmean – 1)/(SG – 1)], where CSG is the specific gravity–corrected biomarker concentration, SGmean is the specific gravity arithmetic mean in our population,
 and C is the measured biomarker concentration. */
   *----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
 sum EVP_168H_SG  
 global SG_mean = r(mean) 
 
    foreach biom in TRICLOSAN BPA {
		gen EVP_168H_SG_`biom' = EVP_168H_`biom'*(($SG_mean-1) / (EVP_168H_SG - 1))
  }

  
  
 *  Average of the creatinine corrected concentrations (Braun et al 2014, BPA obesity) 
 *---------------------------------------------------------------------------------------

 foreach biom in TRICLOSAN BPA  {
		forvalues i = 1(1)72 {
			gen `biom'_creat`i' = `biom'`i'/ CREATININE`i'
	}
	egen mean_creat_168H_`biom' = rowmean(`biom'_creat*)
}
	 
	 
	 
 *   Creatinine based pools based  (Weinberg 2019)
 *----------------------------------------------------

foreach biom in TRICLOSAN BPA  {
	forvalues i = 1(1)72 {
		gen Q_`biom'_W`i' = `biom'`i'* V_pool_W`i'
	}
		
	egen Q_WP_168H_`biom'_T = rowtotal(Q_`biom'_W`i'*)
	egen V_WP_168H_T = rowtotal(V_pool_W`i'*)
	gen WP_168H_`biom' = Q_WP_168H_`biom'_T/V_WP_168H_T 
	label var WP_168H_`biom' "conc in Weinberg pool"

	drop Q_`biom'_W`i'*  V_WP_168H_T Q_WP_168H_`biom'_T 
	corr WP_168H_`biom' G_168H_`biom'
} 
 
*-----------------------------------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
 * B) Creation of derivated variables 
 *-----------------------------------------------------
 *----------------------------------------------------------------------------------------------------------------------------------------------------------

 
 * B.1)  ln_conc 
*--------------------------

* a) TCS and BPA
  foreach biom in TRICLOSAN BPA  {
	 foreach samp in EVP_168H VBP_168H G_168H  EVP_168H_creat  EVP_168H_SG mean_creat_168H{
		gen ln`biom'_`samp' = log(`samp'_`biom')
	 }
}
 
 
*b) Creat and SG 
  foreach biom in SG CREATININE {
	 foreach samp in EVP_168H VBP_168H G_168H  {
		gen ln`biom'_`samp' = log(`samp'_`biom')
	 }
}
 
 * B.2)  Difference  (not use in the mansucript) 
*--------------------------

 * Different in urinary concetration betwwen glod standard and equal volume pool 
 foreach biom in TRICLOSAN BPA CREATININE SG {
	gen diff`biom'_168H_equalvol = ((EVP_168H_`biom' - G_168H_`biom')*100) / G_168H_`biom'
}


 foreach biom in TRICLOSAN BPA CREATININE SG {
	gen absdiff`biom' = abs(G_168H_`biom' - EVP_168H_`biom')
}


 * Different in urinary concetration betwwen glod standard and CBP 
 
 foreach biom in TRICLOSAN BPA  {
	gen diff`biom'_168H_WP = ((WP_168H_`biom' - G_168H_`biom')*100) / G_168H_`biom'
}


 foreach biom in TRICLOSAN BPA  {
	gen absdiff`biom'_WP = abs(WP_168H_`biom' - EVP_168H_`biom')
}


* generate prop of each samples in the eqaul volume pool 
gen prop_S_EVP_168H`i' = 1/N_sample

*---------------------------------------------
 

save "tempo/Gold_168h.dta", replace 


*------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
 * C.  Descriptive analysis 
 *-----------------------------------------------------
*------------------------------------------------------------------------------------------------------------------------------

*--------------------------------------------------------
*For each individul we compared conc in the EVP and gold standard 
*--------------------------------------------------------

* Actual level (data not showed in the manuscript)
* TRICLOSAN / BPA 
 sort ident
list ident G_168H_TRICLOSAN  EVP_168H_TRICLOSAN diffTRICLOSAN_168H_equalvol WP_168H_TRICLOSAN diffTRICLOSAN_168H_WP  G_168H_BPA EVP_168H_BPA diffBPA_168H_equalvol WP_168H_BPA diffBPA_168H_WP 
preserve 
	sort ident 
	keep ident G_168H_TRICLOSAN  EVP_168H_TRICLOSAN diffTRICLOSAN_168H_equalvol  G_168H_BPA EVP_168H_BPA diffBPA_168H_equalvol G_168H_SG EVP_168H_SG diffSG_168H_equalvol G_168H_CREATININE EVP_168H_CREATININE  diffCREATININE_168H_equalvol WP_168H_TRICLOSAN diffTRICLOSAN_168H_WP  WP_168H_BPA diffBPA_168H_WP 
	order  ident G_168H_TRICLOSAN  EVP_168H_TRICLOSAN diffTRICLOSAN_168H_equalvol  WP_168H_TRICLOSAN diffTRICLOSAN_168H_WP G_168H_BPA EVP_168H_BPA diffBPA_168H_equalvol WP_168H_BPA diffBPA_168H_WP G_168H_SG EVP_168H_SG diffSG_168H_equalvol G_168H_CREATININE EVP_168H_CREATININE  diffCREATININE_168H_equalvol
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/des_expo_all_samples_N8.xlsx", firstrow(variables) replace 
restore 


*--------------------------------------------------------
* DATA extraction for Figure 1, starts 
*--------------------------------------------------------

* Extraction reduite for Matthieu Rolland so he can make figure 1 with R (cf program figures.R)  
preserve 
	sort ident 
	keep ident G_168H_TRICLOSAN  EVP_168H_TRICLOSAN  G_168H_BPA EVP_168H_BPA  WP_168H_TRICLOSAN WP_168H_BPA 
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/extraction for Matthieu Figure 1_N8.xlsx", firstrow(variables) replace 
restore 	

*--------------------------------------------------------
* DATA extraction for Figure 1, ends 
*--------------------------------------------------------

* SG,Creat 
list  ident G_168H_SG EVP_168H_SG diffSG_168H_equalvol G_168H_CREATININE EVP_168H_CREATININE  diffCREATININE_168H_equalvol




*--------------------------------------------------------
* Table 2, column 1 and 2, starts 
*Correlation coefficients with gold standard for each exposure proxies (eahc pooling desing) 
*--------------------------------------------------------

foreach biom in TRICLOSAN BPA   {
		local i = 1
	foreach var in EVP_168H EVP_168H_creat EVP_168H_SG WP_168H mean_creat_168H  { 

			spearman G_168H_`biom'  `var'_`biom'
			  if (`i' == 1) {
						  matrix r`biom' = r(rho)

					  }
					 else {
						matrix r`biom' = r`biom'\r(rho)

					 }
				* ---------------------------------------------------------------------------------- *
					 local i = `i'+1
		}	
	}
	
mat Spearman_168H = rBPA,rTRICLOSAN 
matrix colnames Spearman_168H =  BPA TRICLOSAN
matrix rownames  Spearman_168H =  EVP_168H EVP_168H_creat EVP_168H_SG WP_168H mean_creat_168H
mat list Spearman_168H

preserve 
	clear 
	svmat2 Spearman_168H, names(col) rnames(newvar)
	order newvar 
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/correlation with 168H GS.xlsx", firstrow(variables) replace
restore 


*--------------------------------------------------------
* Table 2, column 1 and 2, ends 
*Correlation coefficients with gold standard for each exposure proxies (eahc pooling desing) 
*--------------------------------------------------------

