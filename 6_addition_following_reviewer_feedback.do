/* Answer to reviewer' comments; R1, September 2020 */

/* Answer to Reviewer 2, Q4
	Correlation between EVP-creat-stdz and average of the creatinine-standardized concentrations measured in each spot sample*/ 
	
* Weekly urine collection
* BPA 	
use	"tempo/Gold_168h.dta", clear 
spearman EVP_168H_creat_BPA mean_creat_168H_BPA
spearman EVP_168H_creat_BPA G_168H_BPA 
spearman mean_creat_168H_BPA G_168H_BPA 

scatter EVP_168H_creat_BPA mean_creat_168H_BPA
scatter EVP_168H_creat_BPA G_168H_BPA 
scatter mean_creat_168H_BPA G_168H_BPA 

* Triclosan
spearman EVP_168H_creat_TRICLOSAN mean_creat_168H_TRICLOSAN


* Daily urine collection 	
use "tempo/Gold_24h.dta", clear 

*BPA
spearman EVP_24H_creat_BPA mean_creat_24H_BPA

* Triclosan 
spearman EVP_24H_creat_TRICLOSAN mean_creat_24H_TRICLOSAN
