*----------------------------------------------------------------------------------
*-----------------------------------------------------
* Analysis 3 
* Gold standard =  24-H urine collection
* In this program, we explored the effect of pooling when all the samples were used to make the pool 
*-----------------------------------------------------
*----------------------------------------------------------------------------------





* Open the lond dataset 
use "pooling validation long dataset 2019_09_11_long.dta", clear


* The follwoing variables were dropped because they were either generated when we were considering a full week urine collection or not usefull for the current analysis  
drop V_pool_W 
drop TSLV TSLV_m UFR 


* We generated a variabe that indicates that a sample is from a specific indiv and a specific day 
tostring ident SampleDay, replace  
gen id_id_day = ident + SampleDay
* ok 
* Ge na variable that identifies within day sample number 
bysort id_id_day : gen id_samp_day  = _n 

 * Description of the number of samples per days 
tab id_id_day
* Day 7 of id 2 only have 2 urines (this day id 2 collected 4 urine but 2 had missing volumes and were excluded) 

* We drop this day of urine collection for the 24H_pool because when we will explore the effect of changing the number of urine collected per day, we want to test 2 and 3 samples per days, which won't be possible for this day */ 
drop if id_id_day == "27"



 /* Following section generates variables needed for pool constrution */
 *------------------------------------------------------------------------------

*  gen volume that should be included in the pool for each sample (volume estiamted based on creatinine concentration, CBP, Weinberget al, 2019) 
		centile(CREATININE) if id_id_day == "11"
		gen diff = abs(CREATININE - r(c_1)) if id_id_day ==  "11"
			
		foreach i in 12 13 14 15 16 17 21 22 23 24 25 26 31 32 33 34 35 36 37 41 42 43 44 45 46 47 51 52 53 54 55 56 57 61 62 63 64 65 66 67 71 72 73 74 75 76 77 81 82 83 84 85 86 87   {
			centile(CREATININE) if id_id_day == "`i'"
			replace  diff = abs(CREATININE - r(c_1)) if id_id_day == "`i'"
		}

		bysort id_id_day (diff): gen select = 1 if _n == 1 
		centile  CREATININE if select == 1 & id_id_day ==  "11"
		gen CREAT_ref = r(c_1) if id_id_day ==  "11" 
		centile  TOTAL_VOLUME if select == 1 & id_id_day ==  "11" 
		gen Vol_ref = r(c_1) if id_id_day ==  "11"
	
		foreach i in 12 13 14 15 16 17 21 22 23 24 25 26 31 32 33 34 35 36 37 41 42 43 44 45 46 47 51 52 53 54 55 56 57 61 62 63 64 65 66 67 71 72 73 74 75 76 77 81 82 83 84 85 86 87   {
			centile  CREATININE if select == 1 & id_id_day == "`i'"
			replace  CREAT_ref = r(c_1) if id_id_day == "`i'"
			centile  TOTAL_VOLUME if select == 1 & id_id_day == "`i'"
			replace  Vol_ref = r(c_1) if id_id_day == "`i'"
		}

  
 gen V_pool_W = (Vol_ref*CREAT_ref)/CREATININE 

* check 
list id_id_day samp_number if select ==1
 list V_pool_W Vol_ref CREAT_ref CREATININE if id_id_day =="11" & samp_number == 3
 
 drop CREAT_ref Vol_ref	 diff select /* not usefull anymore */ 
		
		
save "pooling_sensitivity anaylsis_1_GS_24_long.dta", replace 


* change data in wide formet 
reshape wide  Sampletime  sampleID Samplevolume TOTAL_VOLUME samp_number  /*TSLV TSLV_b ln* UFR*/  SG CREATININE BPA TRICLOSAN V_pool_W, i(id_id_day) j(id_samp_day) 
save "pooling_sensitivity anaylsis_1_GS_24_wide.dta", replace 
count 
* 55 lines corresponding to 8invi*7days - Day 7 of id 2 that was discarded because only 2 spots were available for this day 




*-----------------------------------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
*  A) Below we estimated the urinary concentrations in the gold standdard and each type of pools
*-----------------------------------------------------
*-----------------------------------------------------------------------------------------------------------------------------------------------------------


* Gold standard 
*-----------------------------------------------------------------------------------------------------------------------------------------------------------

egen Vol_24h = rowtotal(TOTAL_VOLUME*)
label var Vol_24h "total volume of urine produced over 24h"
list TOTAL_VOLUME* Vol_24h
sum Vol_24h /* cited in the results section */ 


foreach biom in TRICLOSAN BPA CREATININE SG {
	forvalues i = 1(1)14 {
		gen `biom'_Q_`i' = `biom'`i'*TOTAL_VOLUME`i'
	}
		
	egen `biom'_Q = rowtotal(`biom'_Q_*) /* theorical BPA quantity in the 24 hour samples */
	drop `biom'_Q_*

	gen G_24H_`biom' = `biom'_Q / Vol_24h
	label var G_24H_`biom' "Gold standard `biom' conc in 24 hour urine sample"
	drop `biom'_Q 
}

* Volume based pool 
*-----------------------------------------------------------------------------------------------------------------------------------------------------------

* representation of each sample in the final pool according to sample volume and total volume of urine produced : Prop =Vol_samplei / Vol_24h 
forvalues i = 1(1)14 {
	gen prop_S`i' = TOTAL_VOLUME`i' / Vol_24h
}


foreach biom in TRICLOSAN BPA CREATININE SG {
	forvalues i = 1(1)14 {
		gen Q_`biom'_equalvol`i' = `biom'`i'* prop_S`i'
	}
		
	egen VBP_24H_`biom'_ = rowtotal(Q_`biom'_equalvol*)
	label var VBP_24H_`biom' "conc in volume based pool"

	drop Q_`biom'_equalvol*
	corr VBP_24H_`biom' G_24H_`biom'
}



* equal volume pool 
*----------------------------------------------------------------------------------------------------------------------------------------------------------
 

 egen N_sample = rownonmiss(BPA*) /* Step 1:  * Number of urine sample per day */ 
 tab N_sample 
 
 
 foreach biom in TRICLOSAN BPA CREATININE SG {
	 egen `biom'_Q_xML = rowtotal(`biom'*)
	 gen EVP_24H_`biom' = `biom'_Q_xML/N_sample
	 drop `biom'_Q_xML
 }
 
 
 * equal volume pool corrected for creatinine concentration
*----------------------------------------------------------------------------------------------------------------------------------------------------------

   foreach biom in TRICLOSAN BPA {
		gen EVP_24H_creat_`biom' = EVP_24H_`biom' /  EVP_24H_CREATININE
  }
  
  
  
* equal volume pool corrected for SG
*----------------------------------------------------------------------------------------------------------------------------------------------------------
   
 sum EVP_24H_SG  
 global SG_mean = r(mean) 
 
    foreach biom in TRICLOSAN BPA {
		gen EVP_24H_SG_`biom' = EVP_24H_`biom'*(($SG_mean-1) / (EVP_24H_SG - 1))
  }

  
  
 *  creatinine based pool (Weinberg 2019)
 *----------------------------------------------------------------------------------------------------------------------------------------------------------

foreach biom in TRICLOSAN BPA  {
	forvalues i = 1(1)14 {
		gen Q_`biom'_W`i' = `biom'`i'* V_pool_W`i'
	}
		
	egen Q_WP_24H_`biom'_T = rowtotal(Q_`biom'_W`i'*)
	egen V_WP_24H_T = rowtotal(V_pool_W`i'*)
	gen WP_24H_`biom' = Q_WP_24H_`biom'_T/V_WP_24H_T 
	label var WP_24H_`biom' "conc in Weinberg pool"

	drop Q_`biom'_W`i'*  V_WP_24H_T Q_WP_24H_`biom'_T 
} 
  
  
  
 *  Average of the creatinine corrected concentrations (Braun et al 2014, BPA obesity) 
 *----------------------------------------------------------------------------------------------------------------------------------------------------------

 foreach biom in TRICLOSAN BPA  {
		forvalues i = 1(1)14 {
			gen `biom'_creat`i' = `biom'`i'/ CREATININE`i'
	}
	egen mean_creat_24H_`biom' = rowmean(`biom'_creat*)
}
	 
	 
  *---------------------------------------------------------------------------------------------------------------------------------------------------------
 * B) Creation of derivated variables 
 *----------------------------------------------------------------------------------------------------------------------------------------------------------

 
 * B.1)  ln_conc 
*--------------------------

* a) TCS and BPA
  foreach biom in TRICLOSAN BPA  {
	 foreach samp in EVP_24H VBP_24H G_24H  EVP_24H_creat  EVP_24H_SG mean_creat_24H{
		gen ln`biom'_`samp' = log(`samp'_`biom')
	 }
}
 
 
*b) Creat and SG 
  foreach biom in SG CREATININE {
	 foreach samp in EVP_24H VBP_24H G_24H  {
		gen ln`biom'_`samp' = log(`samp'_`biom')
	 }
}
 
 * B.2)  Difference  
*--------------------------

 * Different in urinary concetration betwwen glod standard and equal volume pool 
 foreach biom in TRICLOSAN BPA CREATININE SG {
	gen diff`biom'_24H_equalvol = ((EVP_24H_`biom' - G_24H_`biom')*100) / G_24H_`biom'
}


 foreach biom in TRICLOSAN BPA CREATININE SG {
	gen absdiff`biom' = abs(G_24H_`biom' - EVP_24H_`biom')
}


* generate prop of each samples in the eqaul volume pool 
gen prop_S_EVP_24H`i' = 1/N_sample

*---------------------------------------------
 

save "tempo/Gold_24h.dta", replace 




*------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
 * C.  Descriptive analysis 
 *-----------------------------------------------------
*------------------------------------------------------------------------------------------------------------------------------



*---------------------------------------------
* Figure 2,  Extraction for Matthieu Rolland so he can make figure 2 with the figures.R program 
*---------------------------------------------

preserve 
	keep ident id_id_day  G_24H_TRICLOSAN  EVP_24H_TRICLOSAN  WP_24H_TRICLOSAN G_24H_BPA EVP_24H_BPA  WP_24H_BPA 
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/extraction for Matthieu Figure 2_24H.xlsx", firstrow(variables) replace 
restore 

*---------------------------------------------
* Figure 2,  data exctraction ends 
 *---------------------------------------------




*--------------------------------------------------------
* Description of urinary concetrations in pool and 24 hour urine samples (not shown in the mansucript)
*--------------------------------------------------------

global var_24h_all = "G_24H_TRICLOSAN  EVP_24H_TRICLOSAN EVP_24H_creat_TRICLOSAN EVP_24H_SG_TRICLOSAN mean_creat_24H_TRICLOSAN  WP_24H_TRICLOSAN G_24H_BPA EVP_24H_BPA EVP_24H_creat_BPA EVP_24H_SG_BPA mean_creat_24H_BPA WP_24H_BPA G_24H_CREATININE EVP_24H_CREATININE EVP_24H_SG G_24H_SG"

local i = 1
	foreach var in $var_24h_all  { 
			
			 di " -------------------- Variable `var'`j' -------------------- "
		* ---------------------------------------------------------------------------------- *
				 tabstat `var',  m  stat(n p5 p50 p95 mean sd min max ) save
			 if (`i' == 1) {
				  matrix var_cont = r(StatTotal)'
			  }
			 else {
						matrix temp = r(StatTotal)'
						matrix var_cont=var_cont\temp
			 }
		* ---------------------------------------------------------------------------------- *
			 local i = `i'+1
	}

	matrix colnames var_cont = n p5 p50 p95 mean sd min max	
	matrix list var_cont

	preserve 
	clear
	svmat2 var_cont,  names(col) rnames(newvar)
	order newvar n p5 p50 p95 mean sd min max 
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/24H/des_expo_all_samples_N56_24H.xlsx", firstrow(variables) replace 
	restore 
	
	
	
	
*--------------------------------------------------------
* Table 3, columns 1 et 2 starts 
* Correlation coefficient, 
*--------------------------------------------------------


foreach biom in TRICLOSAN BPA   {
		local i = 1
	foreach var in EVP_24H EVP_24H_creat EVP_24H_SG mean_creat_24H  WP_24H { 

			spearman G_24H_`biom'  `var'_`biom'
			  if (`i' == 1) {
						  matrix r`biom' = r(rho)

					  }
					 else {
						matrix r`biom' = r`biom'\r(rho)

					 }
				* ---------------------------------------------------------------------------------- *
					 local i = `i'+1
		}	
	}
	
mat Spearman_24H = rBPA,rTRICLOSAN 
matrix colnames Spearman_24H =  BPA TRICLOSAN
matrix rownames  Spearman_24H =  EVP_24H EVP_24H_creat EVP_24H_SG mean_creat_24H WP_24H
mat list Spearman_24H

preserve 
	clear 
	svmat2 Spearman_24H, names(col) rnames(newvar)
	order newvar 
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/24H/correlation with 24H GS.xlsx", firstrow(variables) replace
restore 

sort  G_24H_TRICLOSAN
list  G_24H_TRICLOSAN EVP_24H_TRICLOSAN EVP_24H_creat_TRICLOSAN EVP_24H_SG_TRICLOSAN mean_creat_24H_TRICLOSAN WP_24H_TRICLOSAN


foreach biom in CREATININE SG   {
			spearman G_24H_`biom'  EVP_24H_`biom'
	}


*--------------------------------------------------------
* Table 3, columns 1 et 2, ends  
*--------------------------------------------------------
