*----------------------------------------------------------------------------------
*-----------------------------------------------------
* Analysis 2, 
* Gold standard =  urine collection over the week 
* In this program, we explored the effect of pooling when a restricted number of samples were used to make the pools 
*-----------------------------------------------------
*----------------------------------------------------------------------------------


*----------------------------------------------------------------------------------
*-----------------------------------------------------
* A) Simulation of 1000 new databases (in each database the samples selected to be included in the pools will change for each individual 
*-----------------------------------------------------
*----------------------------------------------------------------------------------


forvalues u = 2(1)10 {  /* u = number of samples used to make the pool */


	forvalues s = 1/$nb_simul {

		use "pooling validation long dataset 2019_09_11_long.dta", clear

		* Step 1 : select i sample per indiv to make the pool 
		sample `u', count by(ident) /* we randomly select u samples  per indiv  to make the pool */ 
		tab ident

		
		* Recup total volume overthe full week /* total volume is needed to compute the concentration in the BVP */ 
		preserve
		use "tempo/Gold_168h.dta", clear 
		keep  ident G_168H_BPA G_168H_TRICLOSAN G_168H_SG G_168H_CREATININE 
		 save "tempo/Gold_standard_Variables_168h.dta", replace 
		restore 

		merge m:1 ident using "tempo/Gold_standard_Variables_168h.dta"
		drop _merge 


		* We drop the var we are not interested in to simplify the dataset 
		keep ident  TOTAL_VOLUME /*TSLV TSLV_b ln* UFR*/  SG CREATININE BPA TRICLOSAN G_168H_BPA G_168H_TRICLOSAN G_168H_SG G_168H_CREATININE 

		
		
		/* Correction made below : for some women sampple numbering was not continuous (i.e, one particpant had 6 samples collected, the number of the samples were 1,2,4,5,6,7). 
				This cause us problem in the simulation study for the selection of the samples included in the pool 
				For this reason we correct sample numbering to have no hole, this as no effect for us since we do not care about missing urine void 
				A new variable was generated (newsampid) and the old one was kept in the database  */
		
		bysort ident : gen newsampid = _n 
		
		
		* below we estimated the volume of each spot that should be included in the pool (volume estiamted based on the spots creatinine concentration, Weinberg et al, 2019. CBP) 
		centile(CREATININE) if ident == 1
		gen diff = abs(CREATININE - r(c_1)) if ident ==  1
			
		forvalues i = 2(1)8 {
			centile(CREATININE) if ident == `i'
			replace  diff = abs(CREATININE - r(c_1)) if ident ==  `i' 
		}

		bysort ident (diff): gen select = 1 if _n == 1 
		centile  CREATININE if select == 1 & ident == 1 
		gen CREAT_ref = r(c_1) if ident == 1 
		centile  TOTAL_VOLUME if select == 1 & ident == 1 
		gen Vol_ref = r(c_1) if ident == 1 
	
		forvalues i = 2(1)8 {
			centile  CREATININE if select == 1 & ident == `i' 
			replace  CREAT_ref = r(c_1) if ident == `i' 
			centile  TOTAL_VOLUME if select == 1 & ident ==  `i'
			replace  Vol_ref = r(c_1) if ident ==  `i' 
		}

		drop diff select 

	   gen V_pool_W = (Vol_ref*CREAT_ref)/CREATININE 
	   drop Vol_ref CREAT_ref
	   
	   
* generation of a wide dataset 	   
reshape wide  TOTAL_VOLUME /*TSLV TSLV_b ln* UFR*/  SG CREATININE BPA TRICLOSAN V_pool_W , i(ident) j(newsampid) 




*-----------------------------------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
*  B) Below we estimated the urinary concentrations in the gold standdard and each type of pools
*-----------------------------------------------------
*-----------------------------------------------------------------------------------------------------------------------------------------------------------


		

		* EQV pool 
		*-------------
		 egen N_sample = rownonmiss(BPA*) /* Step 1:  * Number of urine sample per women */ 
		 tab N_sample  
		 
		 foreach biom in TRICLOSAN BPA CREATININE SG {
			 egen `biom'_Q_xML = rowtotal(`biom'*)
			 gen EVP168H_`biom'_`u'= `biom'_Q_xML/N_sample
			label var EVP168H_`biom'_`u' "EVP with `u' samples" 
			 drop `biom'_Q_xML
		 }

		 
		 
		 * EQV pool corrected for creatinine 
		 *-------------------------------------------------------------------------------------
		   foreach biom in TRICLOSAN BPA {
				gen EVP168H_creat_`biom'_`u' = EVP168H_`biom'_`u' / EVP168H_CREATININE_`u'
		  }


		*  EVP corrected for SG 
		*-------------------------------------------------------------------------------------
		 sum EVP168H_SG_`u'
		 global SG_mean = r(mean) 
		 
		 
		  foreach biom in TRICLOSAN BPA {
				gen EVP168H_SG_`biom'_`u' =  EVP168H_`biom'_`u'*(($SG_mean-1) / ( EVP168H_SG_`u' - 1))
		  }
		  
	  

		* Volume based pool 
		*-------------------------------------------------------------------------------------	
		des TOTAL*
		egen Vol_`u'sample = rowtotal(TOTAL_VOLUME*)
		list Vol_`u'sample TOTAL_VOLUME*

		forvalues i = 1(1)`u' {
			gen prop_S`i' = TOTAL_VOLUME`i' / Vol_`u'sample
		}
		list Vol_`u'sample TOTAL_VOLUME* prop_S*


		foreach biom in TRICLOSAN BPA CREATININE SG {
			forvalues i = 1(1)`u' {
				gen Q_`biom'_equalvol`i' = `biom'`i'* prop_S`i'
			}
				
			egen VBP168H_`biom'_`u' = rowtotal(Q_`biom'_equalvol*)
			label var VBP168H_`biom'_`u' "conc in volume based pool, `u' samples"

			drop Q_`biom'_equalvol*
		}


		* Creatinine based pool 
		*-------------------------------------------------------------------------------------
		foreach biom in TRICLOSAN BPA  {
			forvalues i = 1(1)`u' {
				gen Q_`biom'_W`i' = `biom'`i'* V_pool_W`i'
			}
				
			egen Q_WP_168H_`biom'_`u' = rowtotal(Q_`biom'_W`i'*)
			egen V_WP_168H_`u' = rowtotal(V_pool_W`i'*)
			gen WP168H_`biom'_`u' = Q_WP_168H_`biom'_`u'/V_WP_168H_`u' 
			label var WP168H_`biom'_`u' "conc in Weinberg pool"

			drop Q_`biom'_W`i'*  V_WP_168H_`u' Q_WP_168H_`biom'_`u' 
		} 
		 
		
		

		* Average of the 2 creatinine corrected concentrations (Braun et al 2014, BPA obesity) 
		*-------------------------------------------------------------------------------------
		 foreach biom in TRICLOSAN BPA  {
				forvalues i = 1(1)`u' {
					gen `biom'_creat`i' = `biom'`i'/ CREATININE`i'
			}
			egen mean_creat168H_`biom'_`u'  = rowmean(`biom'_creat*)
			drop `biom'_creat*
		}



	/*
		 *  Difference with gold   (not used for the paper)
		*--------------------------
		 * Different in urinary concetration betwwen glod standard and equal volume pool 
		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen diff`biom'_168H_equalvol = ((EVP2_168H_`biom' - G_168H_`biom')*100) / G_168H_`biom'
		}


		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen absdiff`biom' = abs(((EVP2_168H_`biom' - G_168H_`biom')*100) / G_168H_`biom')
		}


		 * Different in urinary concetration betwwen glod standard and equal volume pool 
		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen diff`biom'_168H_VBP2 = ((VBP2_168H_`biom' - G_168H_`biom')*100) / G_168H_`biom'
		}


		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen absdiff`biom'VBP2 = abs(((VBP2_168H_`biom' - G_168H_`biom')*100) / G_168H_`biom')
		}
	*/

		save "tempo/simulation/BS_Sample/Identif_`u'sample_168h_simul_`s'.dta", replace 
	}
}



*------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
 * C.  Descriptive analysis 
 *-----------------------------------------------------
*------------------------------------------------------------------------------------------------------------------------------

*----------------------------------------------------------
* Descriptive statistic not shown in the paper (starts) 
*----------------------------------------------------------

set matsize 10000
	
forvalues u = 2(1)10 {

	forvalues s = 1/$nb_simul {
			use "tempo/simulation/BS_Sample/Identif_`u'sample_168h_simul_`s'.dta", clear 

	local i = 1
		foreach var in EVP168H_TRICLOSAN VBP168H_TRICLOSAN EVP168H_creat_TRICLOSAN EVP168H_SG_TRICLOSAN mean_creat168H_TRICLOSAN WP168H_TRICLOSAN EVP168H_BPA VBP168H_BPA EVP168H_creat_BPA  EVP168H_SG_BPA mean_creat168H_BPA WP168H_BPA EVP168H_CREATININE VBP168H_CREATININE EVP168H_SG VBP168H_SG { 
				
				 di " -------------------- Variable `var'`j' -------------------- "
			* ---------------------------------------------------------------------------------- *
					 tabstat `var'_`u',  m  stat(n p5 p50 p95 mean sd min max ) save
				 if (`i' == 1) {
					  matrix var_cont = r(StatTotal)'
				  }
				 else {
							matrix temp = r(StatTotal)'
							matrix var_cont=var_cont\temp
				 }
			* ---------------------------------------------------------------------------------- *
				 local i = `i'+1
		}

		matrix colnames var_cont = n p5 p50 p95 mean sd min max	
		matrix list var_cont

		preserve 
		clear
		svmat2 var_cont,  names(col) rnames(newvar)
		order newvar n p5 p50 p95 mean sd min max 
		 save "tempo/simulation/BS_Sample/distrib_TCS_BPA_`u'Sample_simul`s'.dta", replace 
		restore 
	}


	use  "tempo/simulation/BS_Sample/distrib_TCS_BPA_`u'Sample_simul1.dta", clear 
	forvalues s = 2/$nb_simul {
		append using "tempo/simulation/BS_Sample/distrib_TCS_BPA_`u'Sample_simul`s'.dta"
	}
	save "tempo/simulation/BS_Sample/distrib_TCS_BPA_`u'Sample_all_simul.dta", replace 
	* Summary statistics (median + IC) 


	foreach var in EVP168H_TRICLOSAN VBP168H_TRICLOSAN EVP168H_creat_TRICLOSAN EVP168H_SG_TRICLOSAN mean_creat168H_TRICLOSAN WP168H_TRICLOSAN EVP168H_BPA VBP168H_BPA EVP168H_creat_BPA EVP168H_SG_BPA mean_creat168H_BPA WP168H_BPA EVP168H_CREATININE VBP168H_CREATININE EVP168H_SG VBP168H_SG { 
		hist  p50 if newvar == "`var'_`u'"
		graph save Graph "$graph/histo/distrib`var'_`u'.gph", replace 
		sum   p50 if newvar ==  "`var'_`u'"
		global p50`var' =  r(mean)
		qui centile (p50) if  newvar ==  "`var'_`u'", centile (2.5 97.5)  
		global inf`var' = r(c_1)
		global sup`var' = r(c_2)

	 }	
  }	

	  
	 mat TRC_EVP168H = $p50EVP168H_TRICLOSAN,$infEVP168H_TRICLOSAN,$supEVP168H_TRICLOSAN
	 mat TRC_VBP168H= $p50VBP168H_TRICLOSAN,$infVBP168H_TRICLOSAN,$supVBP168H_TRICLOSAN
	 mat TRC_EVP168H_creat= $p50EVP168H_creat_TRICLOSAN,$infEVP168H_creat_TRICLOSAN,$supEVP168H_creat_TRICLOSAN
	 mat TRC_EVP168H_SG= $p50EVP168H_SG_TRICLOSAN,$infEVP168H_SG_TRICLOSAN,$supEVP168H_SG_TRICLOSAN
	 mat TRC_mean_creat= $p50mean_creat168H_TRICLOSAN,$infmean_creat168H_TRICLOSAN,$supmean_creat168H_TRICLOSAN
	 mat TRC_WP168H= $p50WP168H_TRICLOSAN,$infWP168H_TRICLOSAN,$supWP168H_TRICLOSAN	
		mat BPA_EVP168H = $p50EVP168H_BPA,$infEVP168H_BPA,$supEVP168H_BPA
	 mat BPA_VBP168H= $p50VBP168H_BPA,$infVBP168H_BPA,$supVBP168H_BPA
	 mat BPA_EVP168H_creat= $p50EVP168H_creat_BPA,$infEVP168H_creat_BPA,$supEVP168H_creat_BPA
	 mat BPA_EVP168H_SG = $p50EVP168H_SG_BPA,$infEVP168H_SG_BPA,$supEVP168H_SG_BPA
	 mat BPA_mean_creat= $p50mean_creat168H_BPA,$infmean_creat168H_BPA,$supmean_creat168H_BPA
	 mat BPA_WP168H= $p50WP168H_BPA,$infWP168H_BPA,$supWP168H_BPA

	 mat CREATININE_EVP168H = $p50EVP168H_CREATININE,$infEVP168H_CREATININE,$supEVP168H_CREATININE
	 mat CREATININE_VBP168H= $p50VBP168H_CREATININE,$infVBP168H_CREATININE,$supVBP168H_CREATININE
	 mat SG_EVP168H = $p50EVP168H_SG,$infEVP168H_SG,$supEVP168H_SG
	 mat SG_VBP168H= $p50VBP168H_SG,$infVBP168H_SG,$supVBP168H_SG
	 
	 
	mat all_median_`u'sample =  TRC_EVP168H\TRC_VBP168H\TRC_EVP168H_creat\TRC_EVP168H_SG\TRC_mean_creat\TRC_WP168H\BPA_EVP168H\BPA_VBP168H\BPA_EVP168H_creat\BPA_EVP168H_SG\BPA_mean_creat\BPA_WP168H\CREATININE_EVP168H\CREATININE_VBP168H\SG_EVP168H\SG_VBP168H

	mat list all_median_`u'sample
	matrix colnames all_median_`u'sample = p50 inf sup
	matrix rownames all_median_`u'sample =  TRC_EVP168H TRC_VBP168H TRC_EVP168H_creat TRC_EVP168H_SG TRC_mean_creat TRC_WP168H BPA_EVP168H BPA_VBP168H BPA_EVP168H_creat BPA_EVP168H_SG BPA_mean_creat BPA_WP168H CREATININE_EVP168H CREATININE_VBP168H SG_EVP168H SG_VBP168H

	preserve 
	clear
	svmat2  all_median_`u'sample, names(col) rnames(newvar) /* This ligne gives TABLE S4 */ 

	qui tostring(inf), gen(stringinf) force format(%7.2f)
	qui tostring(sup), gen(stringsup) force format(%7.2f)
	qui gen IC = "[" + stringinf + " ; " + stringsup + "]" 
	format %7.2f p50
	drop inf sup string*
	order newvar p50 IC 

	save "tempo/simulation/BS_Sample/distrib_TCS_BPA_all_simul_`u'samples.dta", replace
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/P50_IC_`u'_samples_used_for_proxies.xlsx", firstrow(variables) replace 

	restore 

	
*----------------------------------------------------------
* Descriptive statistic not shown in the paper (ends) 
*----------------------------------------------------------	

**--------------------------------------------------------
* Table 2, all columns but 1 and 2, starts 
* Correlation coefficients with gold standard for each exposure proxies 
*--------------------------------------------------------

set matsize 10000
	
	
forvalues u = 2(1)10 {


	foreach biom in TRICLOSAN BPA   {
		foreach var in EVP168H VBP168H EVP168H_creat EVP168H_SG mean_creat168H WP168H { 
		local i = 1
		forvalues s = 1/$nb_simul {
				use "tempo/simulation/BS_Sample/Identif_`u'sample_168h_simul_`s'.dta", clear 
				spearman G_168H_`biom'  `var'_`biom'_`u'
							 di " -------------------- `i' -------------------- "

				  if (`i' == 1) {
							  matrix r`var'_`biom' = r(rho)

						  }
						 else {
							matrix r`var'_`biom'= r`var'_`biom'\r(rho)

						 }
					* ---------------------------------------------------------------------------------- *
						 local i = `i'+1
			}	
		}
	}

	 *----------------------
	* Spearman correlation between gold standard (24 hour urine) and exposure proxies for creat and SG
	*----------------------

	foreach biom in CREATININE SG   {
		foreach var in EVP168H VBP168H { 
		local i = 1
		 forvalues s = 1/$nb_simul {
				use "tempo/simulation/BS_Sample/Identif_`u'sample_168h_simul_`s'.dta", clear 
				spearman G_168H_`biom'  `var'_`biom'_`u'
							 di " -------------------- `i' -------------------- "

				  if (`i' == 1) {
							  matrix r`var'_`biom' = r(rho)

						  }
						 else {
							matrix r`var'_`biom' = r`var'_`biom'\r(rho)

						 }
					* ---------------------------------------------------------------------------------- *
						 local i = `i'+1
			}	
		}
	}



	mat Spearman_tot_`u' = rEVP168H_TRICLOSAN,rVBP168H_TRICLOSAN,rEVP168H_creat_TRICLOSAN,rEVP168H_SG_TRICLOSAN,rmean_creat168H_TRICLOSAN,rWP168H_TRICLOSAN,rEVP168H_BPA,rVBP168H_BPA,rEVP168H_creat_BPA,rEVP168H_SG_BPA,rmean_creat168H_BPA,rWP168H_BPA,rEVP168H_CREATININE,rVBP168H_CREATININE,rEVP168H_SG,rVBP168H_SG
	matrix colnames Spearman_tot_`u' =  rEVP168H_TRICLOSAN_`u'  rVBP168H_TRICLOSAN_`u'  rEVP168H_creat_TRICLOSAN_`u'  rEVP168H_SG_TRICLOSAN_`u'  rmean_creat168H_TRICLOSAN_`u' rWP168H_TRICLOSAN_`u' rEVP168H_BPA_`u'  rVBP168H_BPA_`u'  rEVP168H_creat_BPA_`u'  rEVP168H_SG_BPA_`u'  rmean_creat168H_BPA_`u' rWP168H_BPA_`u'  rEVP168H_CREATININE_`u'  rVBP168H_CREATININE_`u'  rEVP168H_SG_`u'  rVBP168H_SG_`u' 
	mat list Spearman_tot_`u'

	
	* Computation of average correlation and CI 
	*********************************************

	clear
	svmat2 Spearman_tot_`u', names(col) 

	save "tempo/simulation/BS_Sample/correlation_TCS_BPA_all_results_crude_simul_`u'samples.dta", replace


	foreach var in rEVP168H_TRICLOSAN  rVBP168H_TRICLOSAN  rEVP168H_creat_TRICLOSAN  rEVP168H_SG_TRICLOSAN  rmean_creat168H_TRICLOSAN rWP168H_TRICLOSAN  rEVP168H_BPA  rVBP168H_BPA  rEVP168H_creat_BPA  rEVP168H_SG_BPA  rmean_creat168H_BPA rWP168H_BPA rEVP168H_CREATININE  rVBP168H_CREATININE  rEVP168H_SG  rVBP168H_SG {
		hist  `var'_`u' /* Distib pas magnifique??? calcul IC? */ 
		graph save Graph "$graph/histo/corr`var'_`u'.gph", replace 
		sum    `var'_`u'
		global mean`var' =  r(mean)
		centile `var'_`u', centile (2.5 97.5)  
		global inf`var' = r(c_1)
		global sup`var' = r(c_2)

	 }	
 
	 mat TRC_EVP168H_r =$meanrEVP168H_TRICLOSAN,$infrEVP168H_TRICLOSAN,$suprEVP168H_TRICLOSAN
	 mat TRC_VBP168H_r = $meanrVBP168H_TRICLOSAN,$infrVBP168H_TRICLOSAN,$suprVBP168H_TRICLOSAN
	 mat TRC_EVP168H_creat_r =   $meanrEVP168H_creat_TRICLOSAN,$infrEVP168H_creat_TRICLOSAN,$suprEVP168H_creat_TRICLOSAN
	 mat TRC_EVP168H_SG_r =   $meanrEVP168H_SG_TRICLOSAN,$infrEVP168H_SG_TRICLOSAN,$suprEVP168H_SG_TRICLOSAN
	 mat TRC_mean_creat_r =   $meanrmean_creat168H_TRICLOSAN,$infrmean_creat168H_TRICLOSAN,$suprmean_creat168H_TRICLOSAN
	 mat TRC_WP168H_r = $meanrWP168H_TRICLOSAN,$infrWP168H_TRICLOSAN,$suprWP168H_TRICLOSAN
	 mat BPA_EVP168H_r =   $meanrEVP168H_BPA,$infrEVP168H_BPA,$suprEVP168H_BPA
	 mat BPA_VBP168H_r =   $meanrVBP168H_BPA,$infrVBP168H_BPA,$suprVBP168H_BPA
	 mat BPA_EVP168H_creat_r =   $meanrEVP168H_creat_BPA,$infrEVP168H_creat_BPA,$suprEVP168H_creat_BPA
	 mat BPA_EVP168H_SG_r =   $meanrEVP168H_SG_BPA,$infrEVP168H_SG_BPA,$suprEVP168H_SG_BPA
	 mat BPA_mean_creat_r =   $meanrmean_creat168H_BPA,$infrmean_creat168H_BPA,$suprmean_creat168H_BPA
	 mat BPA_WP168H_r =   $meanrWP168H_BPA,$infrWP168H_BPA,$suprWP168H_BPA
	 mat CREATININE_EVP168H_r =   $meanrEVP168H_CREATININE,$infrEVP168H_CREATININE,$suprEVP168H_CREATININE
	 mat CREATININE_VBP168H_r =   $meanrVBP168H_CREATININE,$infrVBP168H_CREATININE,$suprVBP168H_CREATININE
	 mat SG_EVP168H_r =   $meanrEVP168H_SG,$infrEVP168H_SG,$suprEVP168H_SG
	 mat SG_VBP168H_r =   $meanrVBP168H_SG,$infrVBP168H_SG,$suprVBP168H_SG
	 
	 
	 mat all_rho_`u'_sample = TRC_EVP168H_r\TRC_VBP168H_r\TRC_EVP168H_creat_r\TRC_EVP168H_SG_r\TRC_mean_creat_r\TRC_WP168H_r\BPA_EVP168H_r\BPA_VBP168H_r\BPA_EVP168H_creat_r\BPA_EVP168H_SG_r\BPA_mean_creat_r\BPA_WP168H_r\CREATININE_EVP168H_r\CREATININE_VBP168H_r\SG_EVP168H_r\SG_VBP168H_r

	 
	mat list  all_rho_`u'_sample
	matrix colnames all_rho_`u'_sample = mean inf sup
	matrix rownames all_rho_`u'_sample =   TRC_EVP168H_`u'r TRC_VBP168H_`u'r TRC_EVP168H_creat_`u'r TRC_EVP168H_SG_`u'r TRC_mean_creat_`u'r TRC_WP168H_`u'r BPA_EVP168H_`u'r BPA_VBP168H_`u'r BPA_EVP168H_creat_`u'r BPA_EVP168H_SG_`u'r BPA_mean_creat_`u'r BPA_WP168H_`u'r CREATININE_EVP168H_`u'r CREATININE_VBP168H_`u'r SG_EVP168H_`u'r SG_VBP168H_`u'r
 	
		
	clear
	svmat2 all_rho_`u'_sample, names(col) rnames(newvar) /* This ligne gives TABLE S4 */ 

	qui tostring(inf), gen(stringinf) force format(%7.2f)
	qui tostring(sup), gen(stringsup) force format(%7.2f)
	qui gen IC = "[" + stringinf + " ; " + stringsup + "]" 
	format %7.2f mean
	drop inf sup string*
	order newvar mean IC 

	save "tempo/simulation/BS_Sample/correlation_TCS_BPA_all_simul_`u'samples.dta", replace
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/correlation_TCS_BPA_all_simul_`u'samples.xlsx", firstrow(variables) replace 
}


**--------------------------------------------------------
* Table 2, all columns but 1 and 2, ends 
*--------------------------------------------------------



* graph of the distribution of the rho (not shown in the paper) 
foreach var in rEVP168H_TRICLOSAN  rVBP168H_TRICLOSAN  rEVP168H_creat_TRICLOSAN  rEVP168H_SG_TRICLOSAN  rmean_creat168H_TRICLOSAN rWP168H_TRICLOSAN  rEVP168H_BPA  rVBP168H_BPA  rEVP168H_creat_BPA  rEVP168H_SG_BPA  rmean_creat168H_BPA   rWP168H_BPA rEVP168H_CREATININE  rVBP168H_CREATININE  rEVP168H_SG  rVBP168H_SG {
	graph combine "$graph/histo/corr`var'_2.gph" "$graph/histo/corr`var'_3.gph" "$graph/histo/corr`var'_4.gph" "$graph/histo/corr`var'_5.gph" /// 
				  "$graph/histo/corr`var'_6.gph" "$graph/histo/corr`var'_7.gph" "$graph/histo/corr`var'_8.gph" "$graph/histo/corr`var'_9.gph" "$graph/histo/corr`var'_10.gph" , title("`var'")
					graph save Graph "$graph/histo/corr`var'_all.gph", replace 
}

* graph of the distribution of the mediane  (not shown in the paper) 
foreach var in EVP168H_TRICLOSAN  VBP168H_TRICLOSAN  EVP168H_creat_TRICLOSAN  EVP168H_SG_TRICLOSAN  mean_creat168H_TRICLOSAN EVP168H_BPA  VBP168H_BPA  EVP168H_creat_BPA  EVP168H_SG_BPA  mean_creat168H_BPA  EVP168H_CREATININE  VBP168H_CREATININE  EVP168H_SG  VBP168H_SG {
	graph combine "$graph/histo/distrib`var'_2.gph" "$graph/histo/distrib`var'_3.gph" "$graph/histo/distrib`var'_4.gph" "$graph/histo/distrib`var'_5.gph" /// 
				  "$graph/histo/distrib`var'_6.gph" "$graph/histo/distrib`var'_7.gph" "$graph/histo/distrib`var'_8.gph" "$graph/histo/distrib`var'_9.gph" "$graph/histo/distrib`var'_10.gph" , title("`var'")
					graph save Graph "$graph/histo/distrib`var'_all.gph", replace 
}
