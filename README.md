# philippat_comparison_2020

Git repo for [Philippat et Calafat, 2020](https://pubmed.ncbi.nlm.nih.gov/33022216/)

## Comparison of strategies to efficiently combine repeated urine samples in biomarker-based studies 

Code for Philippat et al, Environ Res, 2020 

Analysis were carried out using STATA/SE, version 15.1 (StataCorp, College Station, TX, USA) and R version 4.0.2.

Tables of the paper were generated from the following Stata programs that should be executed in the order detailed below:  
* 1_EVP_validation_study_2020_09_01  
* 2_Weekly_EVP_versus_GS_all samples_2019_11  
* 3_Weekly_EVP_versus_GS_N_samples_varied_2019_11  
* 4_24H_EVP_versus_GS_all samples_2019_11  
* 5_24H_EVP_versus_GS_N_samples_varied_2019_11  
* 6_addition following reviewer feedback  

Figures were obtained from the following R program:  
* figures.R 

Data used for this project are confidential and can be released only after approval by the CDC investigator Antonia M. Calafat. 
 