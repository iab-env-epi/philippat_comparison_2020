
*----------------------------------------------------------------------------------
*-----------------------------------------------------
* Analysis 4 
* Gold standard =  24-H urine collection
* In this program, we explored the effect of pooling when all the samples were used to make the pool 
*-----------------------------------------------------
*----------------------------------------------------------------------------------



forvalues u = 2(1)3 { /* number of spot included in the pool */ 

	forvalues s = 1/$nb_simul {

		use  "pooling_sensitivity anaylsis_1_GS_24_long.dta", clear

		* Step 1 : select i sample per indiv to make the pool 
		sample `u' , count by(id_id_day) /* we randomly select u samples per indiv per visit to make the pool */ 
		tab id_id_day
		bysort id_id_day : replace samp_number  = _n 
		drop id_samp_day /* not needed */ 
		
		* Recup total volume over the 24-H,  total volume is needed to compute the concentration in the volume based pool 
		preserve
		use "tempo/Gold_24h.dta", clear 
		keep id_id_day ident G_24H_BPA G_24H_TRICLOSAN G_24H_SG G_24H_CREATININE 
		 save "tempo/Gold_standard_Variables_24h.dta", replace 
		restore 

		merge m:1 id_id_day ident using "tempo/Gold_standard_Variables_24h.dta"
		drop _merge 


		* We drop the variables that are not usefull  for this analysis to simplify the dataset 
		keep ident id_id_day samp_number TOTAL_VOLUME /*TSLV TSLV_b ln* UFR*/  SG CREATININE BPA TRICLOSAN G_24H_BPA G_24H_TRICLOSAN G_24H_SG G_24H_CREATININE 


		* We identify for each day and participant the ref sample for the creatinine based pool 
		centile(CREATININE) if id_id_day == "11"
		gen diff = abs(CREATININE - r(c_1)) if id_id_day == "11"
			
		foreach i in 12 13 14 15 16 17 21 22 23 24 25 26 31 32 33 34 35 36 37 41 42 43 44 45 46 47 51 52 53 54 55 56 57 61 62 63 64 65 66 67 71 72 73 74 75 76 77 81 82 83 84 85 86 87   {
			centile(CREATININE) if id_id_day  == "`i'"
			replace  diff = abs(CREATININE - r(c_1)) if id_id_day  == "`i'"
		}

		bysort id_id_day (diff): gen select = 1 if _n == 1 
		
		centile  CREATININE if select == 1 & id_id_day == "11" 
		gen CREAT_ref = r(c_1) if id_id_day == "11" 
		centile  TOTAL_VOLUME if select == 1 & id_id_day == "11"
		gen Vol_ref = r(c_1) if id_id_day == "11"
	
		foreach i in 12 13 14 15 16 17 21 22 23 24 25 26 31 32 33 34 35 36 37 41 42 43 44 45 46 47 51 52 53 54 55 56 57 61 62 63 64 65 66 67 71 72 73 74 75 76 77 81 82 83 84 85 86 87   {
			centile  CREATININE if select == 1 & id_id_day  == "`i'" 
			replace  CREAT_ref = r(c_1) if id_id_day  == "`i'" 
			centile  TOTAL_VOLUME if select == 1 & id_id_day  == "`i'"
			replace  Vol_ref = r(c_1) if id_id_day  == "`i'"
		}

		drop diff select 

		* We compouted for each spot the volume that will be used for the pool 
	   gen V_pool_W = (Vol_ref*CREAT_ref)/CREATININE 
	   drop Vol_ref CREAT_ref
	
	
* we generate a wide dataset 
		reshape wide  TOTAL_VOLUME /*TSLV TSLV_b ln* UFR*/  SG CREATININE BPA TRICLOSAN V_pool_W, i(id_id_day) j(samp_number) 



*-----------------------------------------------------------------------------------------------------------------------------------------------------------
*-----------------------------------------------------
*  A) Below we estimated the urinary concentrations in each type of pools
*-----------------------------------------------------
*-----------------------------------------------------------------------------------------------------------------------------------------------------------


		* EQV pool 
		*-------------
		 egen N_sample = rownonmiss(BPA*) /* Step 1:  * Number of urine sample per women */ 
		 tab N_sample  
		 
		 foreach biom in TRICLOSAN BPA CREATININE SG {
			 egen `biom'_Q_xML = rowtotal(`biom'*)
			 gen EVP24H_`biom'_`u'= `biom'_Q_xML/N_sample
			label var EVP24H_`biom'_`u' "EVP with `u' samples" 
			 drop `biom'_Q_xML
		 }

		 
		 
		 * EQV pool corrected for creatinine 
		 *-------------------------------------------------------------------------------------
		   foreach biom in TRICLOSAN BPA {
				gen EVP24H_creat_`biom'_`u' = EVP24H_`biom'_`u' / EVP24H_CREATININE_`u'
		  }


		*  EVP corrected for SG 
		*-------------------------------------------------------------------------------------
		 sum EVP24H_SG_`u'
		 global SG_mean = r(mean) 
		 
		 
		  foreach biom in TRICLOSAN BPA {
				gen EVP24H_SG_`biom'_`u' =  EVP24H_`biom'_`u'*(($SG_mean-1) / ( EVP24H_SG_`u' - 1))
		  }
		  
	  

		* Volume based pool
		*-------------
		* representation of each sample in the final pool according to sample volume and total volume of urine produced : Prop =Vol_samplei / Vol_tot_2 sample  
		* COmputation of the total volume from the 2 selected samples 
		
		des TOTAL*
		egen Vol_`u'sample = rowtotal(TOTAL_VOLUME*)
		list Vol_`u'sample TOTAL_VOLUME*

		forvalues i = 1(1)`u' {
			gen prop_S`i' = TOTAL_VOLUME`i' / Vol_`u'sample
		}
		list Vol_`u'sample TOTAL_VOLUME* prop_S*


		foreach biom in TRICLOSAN BPA CREATININE SG {
			forvalues i = 1(1)`u' {
				gen Q_`biom'_equalvol`i' = `biom'`i'* prop_S`i'
			}
				
			egen VBP24H_`biom'_`u' = rowtotal(Q_`biom'_equalvol*)
			label var VBP24H_`biom'_`u' "conc in volume based pool, `u' samples"

			drop Q_`biom'_equalvol*
		}

	* Creatinine based pool
	*-------------

		foreach biom in TRICLOSAN BPA  {
			forvalues i = 1(1)`u' {
				gen Q_`biom'_W`i' = `biom'`i'* V_pool_W`i'
			}
				
			egen Q_WP_24H_`biom'_`u' = rowtotal(Q_`biom'_W`i'*)
			egen V_WP_24H_`u' = rowtotal(V_pool_W`i'*)
			gen WP24H_`biom'_`u' = Q_WP_24H_`biom'_`u'/V_WP_24H_`u' 
			label var WP24H_`biom'_`u' "conc in Weinberg pool"

			drop Q_`biom'_W`i'*  V_WP_24H_`u' Q_WP_24H_`biom'_`u' 
		} 
		 

		* Average of the 2 creatinine corrected concentrations (Braun et al 2014, BPA obesity) 
		*-------------------------------------------------------------------------------------
		 foreach biom in TRICLOSAN BPA  {
				forvalues i = 1(1)`u' {
					gen `biom'_creat`i' = `biom'`i'/ CREATININE`i'
			}
			egen mean_creat24H_`biom'_`u'  = rowmean(`biom'_creat*)
			drop `biom'_creat*
		}



	/*
		 *  Difference with gold   
		*--------------------------
		 * Different in urinary concetration betwwen glod standard and equal volume pool 
		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen diff`biom'_24H_equalvol = ((EVP2_24H_`biom' - G_24H_`biom')*100) / G_24H_`biom'
		}


		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen absdiff`biom' = abs(((EVP2_24H_`biom' - G_24H_`biom')*100) / G_24H_`biom')
		}


		 * Different in urinary concetration betwwen glod standard and equal volume pool 
		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen diff`biom'_24H_VBP2 = ((VBP2_24H_`biom' - G_24H_`biom')*100) / G_24H_`biom'
		}


		 foreach biom in TRICLOSAN BPA CREATININE SG {
			gen absdiff`biom'VBP2 = abs(((VBP2_24H_`biom' - G_24H_`biom')*100) / G_24H_`biom')
		}
	*/

		save "tempo/simulation/BS_Sample_24H/Identif_`u'sample_24h_simul_`s'.dta", replace 
	}
}


*--------------------------------------------------------
*-----------------------------
	* descriptive analysis
*-----------------------------
*--------------------------------------------------------


* Description of urinary levels (not shown in the paper) 

global var_24H_NSvaried = "EVP24H_TRICLOSAN VBP24H_TRICLOSAN EVP24H_creat_TRICLOSAN EVP24H_SG_TRICLOSAN mean_creat24H_TRICLOSAN WP24H_TRICLOSAN  EVP24H_BPA VBP24H_BPA EVP24H_creat_BPA  EVP24H_SG_BPA mean_creat24H_BPA WP24H_BPA EVP24H_CREATININE VBP24H_CREATININE EVP24H_SG VBP24H_SG"
	
set matsize 10000
	
forvalues u = 2(1)3 {

	forvalues s = 1/$nb_simul {
			use "tempo/simulation/BS_Sample_24H/Identif_`u'sample_24h_simul_`s'.dta", clear 

	local i = 1
		foreach var in $var_24H_NSvaried { 
				
				 di " -------------------- Variable `var'`j' -------------------- "
			* ---------------------------------------------------------------------------------- *
					 tabstat `var'_`u',  m  stat(n p5 p50 p95 mean sd min max ) save
				 if (`i' == 1) {
					  matrix var_cont = r(StatTotal)'
				  }
				 else {
							matrix temp = r(StatTotal)'
							matrix var_cont=var_cont\temp
				 }
			* ---------------------------------------------------------------------------------- *
				 local i = `i'+1
		}

		matrix colnames var_cont = n p5 p50 p95 mean sd min max	
		matrix list var_cont

		preserve 
		clear
		svmat2 var_cont,  names(col) rnames(newvar)
		order newvar n p5 p50 p95 mean sd min max 
		 save "tempo/simulation/BS_Sample_24H/distrib_TCS_BPA_`u'Sample_simul`s'.dta", replace 
		restore 
	}


	use  "tempo/simulation/BS_Sample_24H/distrib_TCS_BPA_`u'Sample_simul1.dta", clear 
	forvalues s = 2/$nb_simul {
		append using "tempo/simulation/BS_Sample_24H/distrib_TCS_BPA_`u'Sample_simul`s'.dta"
	}
	save "tempo/simulation/BS_Sample_24H/distrib_TCS_BPA_`u'Sample_all_simul.dta", replace 
	* Summary statistics (median + IC) 


	foreach var in $var_24H_NSvaried	{ 
		hist  p50 if newvar == "`var'_`u'"
		graph save Graph "$graph/histo/distrib`var'_`u'.gph", replace 
		sum   p50 if newvar ==  "`var'_`u'"
		global p50`var' =  r(mean)
		qui centile (p50) if  newvar ==  "`var'_`u'", centile (2.5 97.5)  
		global inf`var' = r(c_1)
		global sup`var' = r(c_2)

	 }	


	  
	 mat TRC_EVP24H = $p50EVP24H_TRICLOSAN,$infEVP24H_TRICLOSAN,$supEVP24H_TRICLOSAN
	 mat TRC_VBP24H= $p50VBP24H_TRICLOSAN,$infVBP24H_TRICLOSAN,$supVBP24H_TRICLOSAN
	 mat TRC_EVP24H_creat= $p50EVP24H_creat_TRICLOSAN,$infEVP24H_creat_TRICLOSAN,$supEVP24H_creat_TRICLOSAN
	 mat TRC_EVP24H_SG= $p50EVP24H_SG_TRICLOSAN,$infEVP24H_SG_TRICLOSAN,$supEVP24H_SG_TRICLOSAN
	 mat TRC_mean_creat= $p50mean_creat24H_TRICLOSAN,$infmean_creat24H_TRICLOSAN,$supmean_creat24H_TRICLOSAN
	 mat TRC_WP24H= $p50WP24H_TRICLOSAN,$infWP24H_TRICLOSAN,$supWP24H_TRICLOSAN

	
	mat BPA_EVP24H = $p50EVP24H_BPA,$infEVP24H_BPA,$supEVP24H_BPA
	 mat BPA_VBP24H= $p50VBP24H_BPA,$infVBP24H_BPA,$supVBP24H_BPA
	 mat BPA_EVP24H_creat= $p50EVP24H_creat_BPA,$infEVP24H_creat_BPA,$supEVP24H_creat_BPA
	 mat BPA_EVP24H_SG = $p50EVP24H_SG_BPA,$infEVP24H_SG_BPA,$supEVP24H_SG_BPA
	 mat BPA_mean_creat= $p50mean_creat24H_BPA,$infmean_creat24H_BPA,$supmean_creat24H_BPA
	mat BPA_WP24H= $p50WP24H_BPA,$infWP24H_BPA,$supWP24H_BPA

	mat CREATININE_EVP24H = $p50EVP24H_CREATININE,$infEVP24H_CREATININE,$supEVP24H_CREATININE
	 mat CREATININE_VBP24H= $p50VBP24H_CREATININE,$infVBP24H_CREATININE,$supVBP24H_CREATININE
	 mat SG_EVP24H = $p50EVP24H_SG,$infEVP24H_SG,$supEVP24H_SG
	 mat SG_VBP24H= $p50VBP24H_SG,$infVBP24H_SG,$supVBP24H_SG
	 
	 
	mat all_median_`u'sample =  TRC_EVP24H\TRC_VBP24H\TRC_EVP24H_creat\TRC_EVP24H_SG\TRC_mean_creat\TRC_WP24H\BPA_EVP24H\BPA_VBP24H\BPA_EVP24H_creat\BPA_EVP24H_SG\BPA_mean_creat\BPA_WP24H\CREATININE_EVP24H\CREATININE_VBP24H\SG_EVP24H\SG_VBP24H

	mat list all_median_`u'sample
	matrix colnames all_median_`u'sample = p50 inf sup
	matrix rownames all_median_`u'sample =  TRC_EVP24H TRC_VBP24H TRC_EVP24H_creat TRC_EVP24H_SG TRC_mean_creat TRC_WP24H BPA_EVP24H BPA_VBP24H BPA_EVP24H_creat BPA_EVP24H_SG BPA_mean_creat BPA_WP24H CREATININE_EVP24H CREATININE_VBP24H SG_EVP24H SG_VBP24H

	preserve 
	clear
	svmat2  all_median_`u'sample, names(col) rnames(newvar) /* This ligne gives TABLE S4 */ 

	qui tostring(inf), gen(stringinf) force format(%7.2f)
	qui tostring(sup), gen(stringsup) force format(%7.2f)
	qui gen IC = "[" + stringinf + " ; " + stringsup + "]" 
	format %7.2f p50
	drop inf sup string*
	order newvar p50 IC 

	save "tempo/simulation/BS_Sample_24H/distrib_TCS_BPA_all_simul_`u'samples.dta", replace
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/24H/P50_IC_`u'_samples_used_for_proxies.xlsx", firstrow(variables) replace 

	restore 
 }	

*----------------------
* Table 3, alls columns but 1 and 2, starts 
* Spearman correlation between gold standard (24 hour urine) and exposure proxies for BPA and TCS
*----------------------

set matsize 10000
	
forvalues u = 2(1)3 {


	foreach biom in TRICLOSAN BPA   {
		foreach var in EVP24H VBP24H EVP24H_creat EVP24H_SG mean_creat24H WP24H  { 
		local i = 1
		forvalues s = 1/$nb_simul {
				use "tempo/simulation/BS_Sample_24H/Identif_`u'sample_24h_simul_`s'.dta", clear 
				spearman G_24H_`biom'  `var'_`biom'_`u'
							 di " -------------------- `i' -------------------- "

				  if (`i' == 1) {
							  matrix r`var'_`biom' = r(rho)

						  }
						 else {
							matrix r`var'_`biom'= r`var'_`biom'\r(rho)

						 }
					* ---------------------------------------------------------------------------------- *
						 local i = `i'+1
			}	
		}
	}

	 *----------------------
	* Spearman correlation between gold standard (24 hour urine) and exposure proxies for creat and SG
	*----------------------

	foreach biom in CREATININE SG   {
		foreach var in EVP24H VBP24H { 
		local i = 1
		 forvalues s = 1/$nb_simul {
				use "tempo/simulation/BS_Sample_24H/Identif_`u'sample_24h_simul_`s'.dta", clear 
				spearman G_24H_`biom'  `var'_`biom'_`u'
							 di " -------------------- `i' -------------------- "

				  if (`i' == 1) {
							  matrix r`var'_`biom' = r(rho)

						  }
						 else {
							matrix r`var'_`biom' = r`var'_`biom'\r(rho)

						 }
					* ---------------------------------------------------------------------------------- *
						 local i = `i'+1
			}	
		}
	}



	mat Spearman_tot_`u' = rEVP24H_TRICLOSAN,rVBP24H_TRICLOSAN,rEVP24H_creat_TRICLOSAN,rEVP24H_SG_TRICLOSAN,rmean_creat24H_TRICLOSAN,rWP24H_TRICLOSAN,rEVP24H_BPA,rVBP24H_BPA,rEVP24H_creat_BPA,rEVP24H_SG_BPA,rmean_creat24H_BPA,rWP24H_BPA, rEVP24H_CREATININE,rVBP24H_CREATININE,rEVP24H_SG,rVBP24H_SG
	matrix colnames Spearman_tot_`u' =  rEVP24H_TRICLOSAN_`u'  rVBP24H_TRICLOSAN_`u'  rEVP24H_creat_TRICLOSAN_`u'  rEVP24H_SG_TRICLOSAN_`u'  rmean_creat24H_TRICLOSAN_`u' rWP24H_TRICLOSAN_`u' rEVP24H_BPA_`u'  rVBP24H_BPA_`u'  rEVP24H_creat_BPA_`u'  rEVP24H_SG_BPA_`u'  rmean_creat24H_BPA_`u' rWP24H_BPA_`u' rEVP24H_CREATININE_`u'  rVBP24H_CREATININE_`u'  rEVP24H_SG_`u'  rVBP24H_SG_`u' 
	mat list Spearman_tot_`u'

	
	* Computation of average correlation and CI 
	*********************************************

	clear
	svmat2 Spearman_tot_`u', names(col) 

	save "tempo/simulation/BS_Sample_24H/correlation_TCS_BPA_all_results_crude_simul_`u'samples.dta", replace


	foreach var in rEVP24H_TRICLOSAN  rVBP24H_TRICLOSAN  rEVP24H_creat_TRICLOSAN  rEVP24H_SG_TRICLOSAN  rmean_creat24H_TRICLOSAN rWP24H_TRICLOSAN rEVP24H_BPA  rVBP24H_BPA  rEVP24H_creat_BPA  rEVP24H_SG_BPA  rmean_creat24H_BPA rWP24H_BPA  rEVP24H_CREATININE  rVBP24H_CREATININE  rEVP24H_SG  rVBP24H_SG {
		hist  `var'_`u' /* Distib pas magnifique??? calcul IC? */ 
		graph save Graph "$graph/histo/corr`var'_`u'.gph", replace 
		sum    `var'_`u'
		global mean`var' =  r(mean)
		centile `var'_`u', centile (2.5 97.5)  
		global inf`var' = r(c_1)
		global sup`var' = r(c_2)

	 }	
 
	 mat TRC_EVP24H_r =$meanrEVP24H_TRICLOSAN,$infrEVP24H_TRICLOSAN,$suprEVP24H_TRICLOSAN
	 mat TRC_VBP24H_r = $meanrVBP24H_TRICLOSAN,$infrVBP24H_TRICLOSAN,$suprVBP24H_TRICLOSAN
	 mat TRC_EVP24H_creat_r =   $meanrEVP24H_creat_TRICLOSAN,$infrEVP24H_creat_TRICLOSAN,$suprEVP24H_creat_TRICLOSAN
	 mat TRC_EVP24H_SG_r =   $meanrEVP24H_SG_TRICLOSAN,$infrEVP24H_SG_TRICLOSAN,$suprEVP24H_SG_TRICLOSAN
	 mat TRC_mean_creat_r =   $meanrmean_creat24H_TRICLOSAN,$infrmean_creat24H_TRICLOSAN,$suprmean_creat24H_TRICLOSAN
	 mat TRC_WP24H_r = $meanrWP24H_TRICLOSAN,$infrWP24H_TRICLOSAN,$suprWP24H_TRICLOSAN

	 mat BPA_EVP24H_r =   $meanrEVP24H_BPA,$infrEVP24H_BPA,$suprEVP24H_BPA
	 mat BPA_VBP24H_r =   $meanrVBP24H_BPA,$infrVBP24H_BPA,$suprVBP24H_BPA
	 mat BPA_EVP24H_creat_r =   $meanrEVP24H_creat_BPA,$infrEVP24H_creat_BPA,$suprEVP24H_creat_BPA
	 mat BPA_EVP24H_SG_r =   $meanrEVP24H_SG_BPA,$infrEVP24H_SG_BPA,$suprEVP24H_SG_BPA
	 mat BPA_mean_creat_r =   $meanrmean_creat24H_BPA,$infrmean_creat24H_BPA,$suprmean_creat24H_BPA
	mat BPA_WP24H_r =   $meanrWP24H_BPA,$infrWP24H_BPA,$suprWP24H_BPA

	mat CREATININE_EVP24H_r =   $meanrEVP24H_CREATININE,$infrEVP24H_CREATININE,$suprEVP24H_CREATININE
	 mat CREATININE_VBP24H_r =   $meanrVBP24H_CREATININE,$infrVBP24H_CREATININE,$suprVBP24H_CREATININE
	 mat SG_EVP24H_r =   $meanrEVP24H_SG,$infrEVP24H_SG,$suprEVP24H_SG
	 mat SG_VBP24H_r =   $meanrVBP24H_SG,$infrVBP24H_SG,$suprVBP24H_SG
	 
	 
	 mat all_rho_`u'_sample = TRC_EVP24H_r\TRC_VBP24H_r\TRC_EVP24H_creat_r\TRC_EVP24H_SG_r\TRC_mean_creat_r\TRC_WP24H_r\BPA_EVP24H_r\BPA_VBP24H_r\BPA_EVP24H_creat_r\BPA_EVP24H_SG_r\BPA_mean_creat_r\BPA_WP24H_r\CREATININE_EVP24H_r\CREATININE_VBP24H_r\SG_EVP24H_r\SG_VBP24H_r

	 
	mat list  all_rho_`u'_sample
	matrix colnames all_rho_`u'_sample = mean inf sup
	matrix rownames all_rho_`u'_sample =   TRC_EVP24H_`u'r TRC_VBP24H_`u'r TRC_EVP24H_creat_`u'r TRC_EVP24H_SG_`u'r TRC_mean_creat_`u'r TRC_WP24H_`u'r BPA_EVP24H_`u'r BPA_VBP24H_`u'r BPA_EVP24H_creat_`u'r BPA_EVP24H_SG_`u'r BPA_mean_creat_`u'r BPA_WP24H_`u'r CREATININE_EVP24H_`u'r CREATININE_VBP24H_`u'r SG_EVP24H_`u'r SG_VBP24H_`u'r
 	
		
	clear
	svmat2 all_rho_`u'_sample, names(col) rnames(newvar) /* This ligne gives TABLE S4 */ 

	qui tostring(inf), gen(stringinf) force format(%7.2f)
	qui tostring(sup), gen(stringsup) force format(%7.2f)
	qui gen IC = "[" + stringinf + " ; " + stringsup + "]" 
	format %7.2f mean
	drop inf sup string*
	order newvar mean IC 

	save "tempo/simulation/BS_Sample_24H/correlation_TCS_BPA_all_simul_`u'samples.dta", replace
	export excel using "/Users/claire/Documents/Pooling validation study/Results/tempo/24H/correlation_TCS_BPA_all_simul_`u'samples.xlsx", firstrow(variables) replace 
}



*----------------------
* Table 3, alls columns but 1 and 2,stops  
* Spearman correlation between gold standard (24 hour urine) and exposure proxies for BPA and TCS
*----------------------



* Below are additional analysis that were not displayed in the paper 
*-------------------------------------------------------------------------



* graph of the distribution of the rho 
foreach var in rEVP24H_TRICLOSAN  rVBP24H_TRICLOSAN  rEVP24H_creat_TRICLOSAN  rEVP24H_SG_TRICLOSAN  rmean_creat24H_TRICLOSAN rEVP24H_BPA  rVBP24H_BPA  rEVP24H_creat_BPA  rEVP24H_SG_BPA  rmean_creat24H_BPA  rEVP24H_CREATININE  rVBP24H_CREATININE  rEVP24H_SG  rVBP24H_SG {
	graph combine "$graph/histo/corr`var'_2.gph" "$graph/histo/corr`var'_3.gph" /*"$graph/histo/corr`var'_4.gph" "$graph/histo/corr`var'_5.gph" /// 
				  "$graph/histo/corr`var'_6.gph" "$graph/histo/corr`var'_7.gph" "$graph/histo/corr`var'_8.gph" "$graph/histo/corr`var'_9.gph" "$graph/histo/corr`var'_10.gph" */, title("`var'")
					graph save Graph "$graph/histo/corr`var'_all.gph", replace 
}

* graph of the distribution of the mediane  
foreach var in EVP24H_TRICLOSAN  VBP24H_TRICLOSAN  EVP24H_creat_TRICLOSAN  EVP24H_SG_TRICLOSAN  mean_creat24H_TRICLOSAN EVP24H_BPA  VBP24H_BPA  EVP24H_creat_BPA  EVP24H_SG_BPA  mean_creat24H_BPA  EVP24H_CREATININE  VBP24H_CREATININE  EVP24H_SG  VBP24H_SG {
	graph combine "$graph/histo/distrib`var'_2.gph" "$graph/histo/distrib`var'_3.gph" /*"$graph/histo/distrib`var'_4.gph" "$graph/histo/distrib`var'_5.gph" /// 
				  "$graph/histo/distrib`var'_6.gph" "$graph/histo/distrib`var'_7.gph" "$graph/histo/distrib`var'_8.gph" "$graph/histo/distrib`var'_9.gph" "$graph/histo/distrib`var'_10.gph" */, title("`var'")
					graph save Graph "$graph/histo/distrib`var'_all.gph", replace 
}



* Proportion of samples included in VBP

use "tempo/simulation/BS_Sample_24H/Identif_2sample_24h_simul_1.dta", clear
hist prop_S1
centile prop*, centile(10,50,90)

use "tempo/simulation/BS_Sample_24H/Identif_2sample_24h_simul_2.dta", clear
hist prop_S1
centile prop*, centile(10,50,90)
 spearman EVP24H_BPA_2 VBP24H_BPA_2
 scatter EVP24H_BPA_2 VBP24H_BPA_2
